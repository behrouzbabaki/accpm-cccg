#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cassert>
#include <iomanip>

#include "instance.hpp"

using std::vector;
using std::string;
using std::ifstream;
using std::cout;
using std::endl;
using std::ios;
using std::istringstream;
using std::make_pair;
using std::pair;
using std::cerr;
using std::setprecision;

Instance::Instance(const char* instance_file, 
                  const char* constraint_file,
                  const char* initial_solution_file) {
    read_instance(instance_file);
    read_constraints(constraint_file);
    read_initial_clusters(initial_solution_file);
}

void Instance::read_instance(const char* fileName)
{
    ifstream f;
    istringstream ss;
    string str;

    _coordinates.clear();
    f.open(fileName);
    if (f.is_open())
    {
        getline(f, str);
        ss.clear();
        ss.str(str);
        vector<double> values;
        double value;
        while (ss >> value)
            values.push_back(value);
        _coordinates.push_back(values);
        _dimension = values.size();
        ss.clear();
        getline(f,str);
        while (f.good())
        {
            values.clear();
            ss.clear();
            ss.str(str);
            while (ss >> value)
                values.push_back(value);
            if (!values.empty())
                _coordinates.push_back(values);
            getline(f,str);
        }
    }
    else
    {
        cerr << "Error! Can't open instance file '" 
             << fileName << "', quitting...\n";
        exit(EXIT_FAILURE);
    }

    _number_of_elements = _coordinates.size();
    _distances.clear();
    for (size_t i = 0; i < _number_of_elements; i++)
    {
        vector<double> current_distances;
        for (size_t j = 0; j < _number_of_elements; j++) {
            size_t dim = _coordinates[i].size();
            if (dim != _coordinates[j].size()) {
                cerr << "different dimensions" << endl;
                exit (EXIT_FAILURE);
            }
            double total_distance = 0, distance = 0;
            for (size_t k = 0; k < dim; k++) {
                distance = _coordinates[i][k] - _coordinates[j][k];
                total_distance += distance * distance;
            }
            current_distances.push_back(total_distance);            
        }
        _distances.push_back(current_distances);
    }    
}

double getClusterCost(const vector< vector< double > >& distance, 
                      const vector< bool >& cluster)
{
   double cluster_cost = 0;
   size_t n_instances = cluster.size();

   size_t cluster_size = 0;
   for (size_t i = 0 ; i < n_instances; i++)
      if (cluster[i])
        cluster_size++;
   
   for (size_t i = 0 ; i < n_instances; i++)
      for (size_t j = i + 1; j < n_instances; j++)
         if (cluster[i] && cluster[j])
            cluster_cost += distance[i][j];

   if (cluster_size > 0)
     cluster_cost /= cluster_size;
   return cluster_cost;
}

void Instance::read_initial_clusters(const char* fileName)
{

    _initial_clusters.clear();
    ifstream inputFile (fileName, ios::in);
    if (inputFile.fail())
    {
        cerr << "Error! Can't open cluster file '" 
             << fileName << "', quitting...\n";
        exit(EXIT_FAILURE);
    }

    int line = -1;
    string s;
    while(getline(inputFile,s))
    {
        if (s.empty())
            continue;
        line += 1;
        int clusIndex = atoi(s.c_str());
        if (clusIndex < 0)
        {
            cerr << "Error reading '" << fileName 
                    << "', found negative cluster index '" 
                    << clusIndex << "'\n";
            exit(EXIT_FAILURE);
        }
        if (clusIndex > (int)_initial_clusters.size()-1)
            _initial_clusters.resize(clusIndex+1, vector<bool>(_number_of_elements, false));
        _initial_clusters[clusIndex][line] = true;
    }
    size_t number_of_clusters = _initial_clusters.size();

    for (int counter = 0; counter < number_of_clusters; counter++)
        assert (_initial_clusters[counter].size() == (unsigned)_number_of_elements);

    _initial_costs.clear();
    for (int counter = 0; counter < number_of_clusters; counter++)
        _initial_costs.push_back(getClusterCost(_distances, _initial_clusters[counter]));
}

void Instance::read_constraints(const char* fileName)
{
    _ml_pairs.clear();
    _cl_pairs.clear();
    ifstream inputFile (fileName, ios::in);

    if (inputFile.fail())
    {
        cerr << "Error! Can't open constraint file '" 
             << fileName << "', quitting...\n";
        exit(EXIT_FAILURE);
    }

    string line;
    while(getline(inputFile, line))
    {
        istringstream ss(line);
        unsigned int first, second;
        int consType;
        ss >> first >> second >> consType;
        if (!ss.fail())
        {
            pair<unsigned int, unsigned int> p =
                make_pair(first, second);
            if (consType == 1)
                _ml_pairs.push_back(p);
            else if (consType == -1)
                _cl_pairs.push_back(p);
        }
    }
}

vector<vector<double> > 
Instance::get_distances(void){
    return _distances;
}

int Instance::get_number_of_instances(void){
    return _number_of_elements;
}

int Instance::get_dimension(void){
    return _dimension;
}

vector<vector<double> > 
Instance::get_coordinates(void){
    return _coordinates;
}

vector< pair< uint, uint > > 
Instance::get_ml_pairs(void){
    return _ml_pairs;
}

vector< pair< uint, uint > > 
Instance::get_cl_pairs(void){
    return _cl_pairs;
}

vector< vector< bool > > 
Instance::get_initial_clusters(void){
    return _initial_clusters;
}

vector< double > 
Instance::get_initial_costs(void){
    return _initial_costs;
}