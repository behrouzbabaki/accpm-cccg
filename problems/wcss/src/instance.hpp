#pragma once

#include <string>
#include <vector>
#include <utility>

using std::string;
using std::vector;
using std::pair;

class Instance{

public:
    Instance(const char*, const char*, const char*);
    vector<vector<double> > get_distances(void);
    int get_number_of_instances(void);
    int get_dimension(void);
    vector<vector<double> > get_coordinates(void);
    vector< pair< uint, uint > > get_ml_pairs(void);
    vector< pair< uint, uint > > get_cl_pairs(void);
    vector< vector< bool > > get_initial_clusters(void);
    vector< double > get_initial_costs(void);        

private:
    unsigned _number_of_elements;
    int _dimension;

    vector< vector< double > > _coordinates;
    vector< vector< double > > _distances;
    vector< pair< uint, uint > > _ml_pairs;
    vector< pair< uint, uint > > _cl_pairs;
    vector< vector< bool > > _initial_clusters;
    vector< double > _initial_costs;
    
    void read_instance(const char*);
    void read_constraints(const char*);
    void read_initial_clusters(const char*);
};