#pragma once

#include <utility>
#include <vector>

using std::vector;
using std::pair;

struct cfCandidateBlock {
  int block;
  double quality;
  double distance_sum;
};

struct cfSolution {
  double quality;
  vector< int > cluster;
  
  cfSolution(double _quality, vector< int > _cluster) 
    : quality(_quality), cluster(_cluster) {};
  
  void print();
};

class ClusterFinder {

public:
  
  ClusterFinder (const vector< pair< uint, uint > >&, 
                 const vector< pair< uint, uint > >&, 
                 const vector< vector< double > >&);
  void solve(const vector< double >&, double);
  bool hasSolution(void) const;
  double getSolution(vector< bool >&) const;
  double getSolObj(vector< bool >&) const;  
  void getSolutions(vector< double >&, vector< vector< bool > >&) const;
  void getSolObjs(vector< double >&, vector< vector< bool > >&) const;
  
  
private:

  void check_dist();
  void pruning_power_lambdas();
  void set_compatibility();
  void check_best(vector< int >&, const cfCandidateBlock&);
  void calc_dist_bound (const vector< cfCandidateBlock >&);
  bool prune0 (vector< int >& , const vector< cfCandidateBlock >&, double, double);
  bool prune1 (vector< int >& , const vector< cfCandidateBlock >&, double, double);
  bool prune2 (vector< int >& , const vector< cfCandidateBlock >&, double, double);
  bool prune3 (vector< int >& , const vector< cfCandidateBlock >&, double, double);
  void search (vector< int >& , const vector< cfCandidateBlock >&, int, double, double);
  void search (vector< int >& , const vector< cfCandidateBlock >&, double, double);
  int getSize() const;
  double getDist(int, int);
  double getPointDist(int, int);
  double getBlockDist(int, int);
  void computeBlockDist ();
  void initialize_lambSum();
  void initialize_within_distances ();
  int getIndex(int, int, int);
  void create_mlBlocks(void);
  void initialize_mlBlocks(void);
  bool checkCompatibility ( int i, int j );

  vector< vector< double > > _distances;
  vector< double > _lambdas;
  double _sigma;

  vector< vector< int > > _compatibility;
  vector< double > _dist_bound;
  vector< cfSolution > _best_clusters;
  vector< pair< uint, uint > > _inputMlPairs;
  vector< uint > _inputMlBlocks;
  vector< uint > _mlBlockIndices;
  vector< pair< uint, uint > > _inputClPairs;
  vector< double > _within_dist;
  vector< double > _block_lambSum;
  vector< vector< uint > > _mlBlocks;
  vector< double > _block_distances;
  vector< vector< double > > _point_block_distances; 


  double _best;
  bool _mlBlocksInitialized;
  int _size;
  uint _numberOfMlBlocks;

};

bool cf_comp2 ( const cfCandidateBlock &a, const cfCandidateBlock &b );
bool cf_comp3 ( const cfCandidateBlock &a, const cfCandidateBlock &b ); 
