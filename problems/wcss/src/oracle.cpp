#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <map>

#include "oracle.hpp"

#define CF_SAFE_GT(a, b) ((a) > (b))
#define CF_SAFE_LT(a, b) ((a) < (b))

using std::cout;
using std::endl;
using std::isfinite;
using std::make_pair;
using std::map;
using std::pair;
using std::swap;
using std::vector;

bool cf_comp2(const cfCandidateBlock &a, const cfCandidateBlock &b)
{
  return a.distance_sum > b.distance_sum;
}

bool cf_comp3(const cfCandidateBlock &a, const cfCandidateBlock &b)
{
  return a.quality < b.quality;
}

void ClusterFinder::create_mlBlocks()
{
    vector< int > tempMBs(_size, -1);
    int blockNum = 0;
    for (auto ml : _inputMlPairs)
    {
        uint first = ml.first;
        uint second = ml.second;
        int firstInd = tempMBs[first], secondInd = tempMBs[second];
        if (firstInd == -1 && secondInd == -1)
        {
            tempMBs[first] = blockNum;
            tempMBs[second] = blockNum;
            blockNum++;
        }
        else if (firstInd > -1 && secondInd > -1)
        {
            for (auto itr = tempMBs.begin(), endItr = tempMBs.end(); itr != endItr; itr++)
                if (*itr == firstInd)
                    *itr = secondInd;
        }
        else
        {
            if (firstInd == -1)
                tempMBs[first] = secondInd;
            else
                tempMBs[second] = firstInd;
        }
    }

    _inputMlBlocks.clear();
    for (auto itr = tempMBs.begin(), endItr = tempMBs.end(); itr != endItr; itr++)
    {
        if (*itr == -1)
            *itr = blockNum++;
        _inputMlBlocks.push_back(*itr);
    }
}

void ClusterFinder::initialize_mlBlocks()
{
  _mlBlocks.clear();
  _mlBlockIndices.resize(_size, -1);
  map< uint, uint > indexMap;
  for (size_t counter = 0, size = _inputMlBlocks.size(); counter < size; counter++)
  {
    map< uint, uint >::iterator itr = indexMap.find(_inputMlBlocks[counter]);
    if (itr == indexMap.end())
    {
      vector< uint > vec;
      vec.push_back(counter);
      _mlBlocks.push_back(vec);
      indexMap[_inputMlBlocks[counter]] = _mlBlocks.size() - 1;
      _mlBlockIndices[counter] = _mlBlocks.size() - 1;
    }
    else
    {
      uint index = itr->second;
      _mlBlocks[index].push_back(counter);
      _mlBlockIndices[counter] = index;
    }
  }
  this->_numberOfMlBlocks = _mlBlocks.size();
  this->_mlBlocksInitialized = true;
}

bool ClusterFinder::checkCompatibility(int i, int j)
{
  if ((_within_dist[i] + _within_dist[j] + getBlockDist(i, j)) 
      / (double)(_mlBlocks[i].size() + _mlBlocks[j].size()) > 
      _block_lambSum[i] + _block_lambSum[j])
    return false;
  else
    return true;
}

void ClusterFinder::set_compatibility()
{
  if (!_mlBlocksInitialized)
    this->initialize_mlBlocks();

  _compatibility.clear();
  _compatibility.resize(_numberOfMlBlocks);
  for (size_t counter = 0; counter < _numberOfMlBlocks; counter++)
    _compatibility[counter].resize(_numberOfMlBlocks, 1);

  for (auto itr = _inputClPairs.begin(), endItr = _inputClPairs.end(); itr != endItr; itr++)
  {
    uint firstBlock, secondBlock;
    firstBlock = _mlBlockIndices[itr->first];
    secondBlock = _mlBlockIndices[itr->second];
    _compatibility[firstBlock][secondBlock] = _compatibility[secondBlock][firstBlock] = 0;
  }

  for (size_t counter1 = 0; counter1 < _numberOfMlBlocks; counter1++)
    for (size_t counter2 = 0; counter2 < _numberOfMlBlocks; counter2++)
      _compatibility[counter1][counter2] = _compatibility[counter2][counter1] =
          _compatibility[counter2][counter1] && checkCompatibility(counter1, counter2) && 
           !(_mlBlocks[counter1].size() == 1 && 
             _mlBlocks[counter2].size() == 1 &&
             (sqrt(getBlockDist(counter1, counter2)) > 
              sqrt(_block_lambSum[counter1]) 
              + sqrt(_block_lambSum[counter2])
             )
           );
}

bool ClusterFinder::hasSolution() const
{
  return (!_best_clusters.empty());
}

void ClusterFinder::check_best(vector< int > &cluster, const cfCandidateBlock &candidate)
{
  if (_best - candidate.quality > 0.000001)
  { 
    _best = candidate.quality;
    for (auto itr = _mlBlocks[candidate.block].begin(), endItr = _mlBlocks[candidate.block].end(); itr != endItr; itr++)
      cluster.push_back(*itr);
    _best_clusters.push_back(cfSolution(_best, cluster));

    for (size_t counter = 0; counter < _mlBlocks[candidate.block].size(); counter++)
      cluster.pop_back();
  }
}

void ClusterFinder::calc_dist_bound(const vector< cfCandidateBlock > &candidates)
{
  vector< double > dist_list;
  for (auto x = candidates.begin(); x != candidates.end(); ++x)
    for (auto y = candidates.begin(); y < x; ++y)
    {
      int size = (_mlBlocks[x->block].size() * _mlBlocks[y->block].size());
      double score = _point_block_distances[x->block][y->block] / size;
      for (int i = 0; i < size; ++i)
        dist_list.push_back(score);
    }
  sort(dist_list.begin(), dist_list.end());

  int pos = 0;
  int size = 0;
  for (auto itr = candidates.begin(), endItr = candidates.end(); itr != endItr; itr++)
    size += _mlBlocks[itr->block].size();

  for (int i = 0; i < size; ++i)
  {
    double sum = 0;
    for (int j = 0; j < i; ++j)
    {
      sum += dist_list[pos];
      ++pos;
    }
    _dist_bound.push_back(sum);
  }
}

bool ClusterFinder::prune1(vector< int > &in_cluster, 
                           const vector< cfCandidateBlock > &candidates, 
                           double distance_sum, double lambda_sum)
{
  static vector< double > lambda_values;
  static vector< double > distance_values;

  lambda_values.resize(0);
  distance_values.resize(0);

  double new_lambda_sum = lambda_sum;
  double new_distance_sum = distance_sum;
  int new_size = in_cluster.size();

  for (auto x = candidates.begin(); x != candidates.end(); ++x)
  {
    int blockSize = _mlBlocks[x->block].size();
    double avgLamb = _block_lambSum[x->block] / blockSize;
    double avgDist = x->distance_sum / blockSize;

    for (int blockpointCount = 0; blockpointCount < blockSize; blockpointCount++)
    {
      distance_values.push_back(avgDist);
      lambda_values.push_back(avgLamb);
    }
  }

  sort(lambda_values.rbegin(), lambda_values.rend());
  sort(distance_values.begin(), distance_values.end());

  int pointCount = 0;
  for (auto itr = candidates.begin(), endItr = candidates.end(); itr != endItr; itr++)
  {
    int blockNumber = itr->block;

    for (size_t blockpointCount = 0, nblockpoints = _mlBlocks[blockNumber].size(); 
         blockpointCount < nblockpoints; blockpointCount++, pointCount++)
    {
      new_lambda_sum += lambda_values[pointCount];
      new_distance_sum += distance_values[pointCount];
      new_size++;
      if (CF_SAFE_LT((new_distance_sum / new_size) - new_lambda_sum, _best))
        return false;
    }
  }

  return true;
}

void ClusterFinder::search(vector< int > &in_cluster, 
                           const vector< cfCandidateBlock > &candidates, 
                           int i, double distance_sum, double lambda_sum)
{
  const cfCandidateBlock &x = candidates[i];
  for (auto itr = _mlBlocks[x.block].begin(), endItr = _mlBlocks[x.block].end(); itr != endItr; itr++)
    in_cluster.push_back(*itr);

  vector< cfCandidateBlock > new_candidates;
  new_candidates.reserve(candidates.size()); 
  cfCandidateBlock c;
  int new_size;
  lambda_sum += _block_lambSum[x.block];
  distance_sum += x.distance_sum;

  for (int j = i + 1, size = candidates.size(); j < size; ++j)
  {
    c.block = candidates[j].block;
    if (_compatibility[x.block][c.block] == 0)
      continue;
    c.distance_sum = candidates[j].distance_sum + getBlockDist(x.block, c.block);
    new_size = in_cluster.size() + _mlBlocks[c.block].size();
    c.quality = (distance_sum + c.distance_sum) / new_size - lambda_sum - _block_lambSum[c.block];

    new_candidates.push_back(c);
    check_best(in_cluster, c);
  }

  sort(new_candidates.begin(), new_candidates.end(), cf_comp2);
  search(in_cluster, new_candidates, distance_sum, lambda_sum);

  for (size_t counter = 0, blockSize = _mlBlocks[x.block].size(); counter < blockSize; counter++)
    in_cluster.pop_back();
}

void ClusterFinder::search(vector< int > &in_cluster, 
                           const vector< cfCandidateBlock > &candidates, 
                           double distance_sum, double lambda_sum)
{
  int size = candidates.size();
  double bound = 0.0;
  for (int i = 0; i < size; ++i)
    bound += _block_lambSum[candidates[i].block];
  if (CF_SAFE_GT(distance_sum / in_cluster.size() - lambda_sum - bound, _best))
    return;
  if (prune1(in_cluster, candidates, distance_sum, lambda_sum))
    return;
  for (int i = 0; i < size; ++i)
  {
    bound -= _block_lambSum[candidates[i].block];
    if (CF_SAFE_LT(candidates[i].quality - bound, _best))
      search(in_cluster, candidates, i, distance_sum, lambda_sum);
  }
}

void ClusterFinder::solve(const vector< double > &lambdas, double sigma)
{
  _lambdas = lambdas;
  _sigma = sigma;
  _best = -sigma;
  this->initialize_lambSum();

  vector< int > in_cluster;
  vector< cfCandidateBlock > candidates;
  set_compatibility();
  cfCandidateBlock c;
  c.distance_sum = 0;

  for (size_t counter = 0; counter < _numberOfMlBlocks; ++counter)
  {
    double blockQuality = _within_dist[counter] / _mlBlocks[counter].size() - _block_lambSum[counter];
    if (blockQuality < 0)
    {
      c.block = counter;
      c.quality = blockQuality;
      c.distance_sum = _within_dist[counter];
      candidates.push_back(c);
      check_best(in_cluster, c);
    }
  }
  sort(candidates.begin(), candidates.end(), cf_comp3);
  search(in_cluster, candidates, 0, 0);
}

ClusterFinder::ClusterFinder(const vector< pair< uint, uint > > &inputMlPairs,
                             const vector< pair< uint, uint > > &inputClPairs,
                             const vector< vector< double > > &distances)
    : _distances(distances),
      _inputMlPairs(inputMlPairs),
      _inputClPairs(inputClPairs),
      _mlBlocksInitialized(false),
      _numberOfMlBlocks(-1)
{
  this->_size = _distances.size();
  this->create_mlBlocks();
  this->initialize_mlBlocks();
  this->initialize_within_distances();
  this->computeBlockDist();
}

double ClusterFinder::getPointDist(int first, int second)
{
  return this->_distances[first][second];
}

double ClusterFinder::getSolObj(vector< bool >&solution) const {
  double quality = getSolution(solution);
  return - quality - _sigma;
}

double ClusterFinder::getSolution(vector< bool > &outSolution) const
{
  outSolution.clear();
  outSolution.resize(getSize(), false);
  const cfSolution &best = _best_clusters.back();
  for (auto itr = best.cluster.begin(); itr != best.cluster.end(); itr++)
    outSolution[*itr] = true;
  return best.quality;
}

void ClusterFinder::getSolObjs(vector< double >& objs,
                               vector< vector< bool > >&solutions) const {
    getSolutions(objs, solutions);
    for (size_t i=0, s=objs.size(); i!=s; i++)
      objs[i] = -objs[i] - _sigma;
}

void ClusterFinder::getSolutions(vector< double > &outQuals,
                                 vector< vector<bool> > &outSolutions) const
{
  size_t nSols = _best_clusters.size();
  outSolutions.clear();
  outSolutions.reserve(nSols);
  outQuals.clear();
  outQuals.reserve(nSols);
  for (size_t i=0; i!=nSols; i++) {
    const cfSolution &cur = _best_clusters[i];
    vector< bool > clus(getSize(), false);
    for (auto itr = cur.cluster.begin(); itr != cur.cluster.end(); itr++)
      clus[*itr] = true;    
    outSolutions.push_back(clus);
    outQuals.push_back(cur.quality);
  }  
}

int ClusterFinder::getSize(void) const
{
  return this->_size;
}

double ClusterFinder::getBlockDist(int firstBlock, int secondBlock)
{
  if (firstBlock == secondBlock)
    return 0;
  if (firstBlock > secondBlock)
    swap(firstBlock, secondBlock);
  return _block_distances[getIndex(firstBlock, secondBlock, _numberOfMlBlocks)];
}

void ClusterFinder::computeBlockDist()
{
  if (!_mlBlocksInitialized)
    initialize_mlBlocks();

  _block_distances.clear();
  _block_distances.resize(_numberOfMlBlocks * (_numberOfMlBlocks - 1) / 2);
  _point_block_distances.resize(_size, vector< double>(_numberOfMlBlocks, 0));
  for (size_t i = 0; i < _numberOfMlBlocks; i++)
    for (size_t j = i + 1; j < _numberOfMlBlocks; j++)
    {
      double total_dist = 0;
      for (auto itr1 = _mlBlocks[i].begin(), endItr1 = _mlBlocks[i].end(); itr1 != endItr1; itr1++)
        for (auto itr2 = _mlBlocks[j].begin(), endItr2 = _mlBlocks[j].end(); itr2 != endItr2; itr2++)
        {
          _point_block_distances[*itr1][j] += getPointDist(*itr1, *itr2);
          total_dist += this->getPointDist(*itr1, *itr2);
        }
      _block_distances[getIndex(i, j, _numberOfMlBlocks)] = total_dist;
    }
}

void ClusterFinder::initialize_lambSum()
{
  if (!_mlBlocksInitialized)
    initialize_mlBlocks();

  _block_lambSum.clear();
  _block_lambSum.reserve(_numberOfMlBlocks);
  for (size_t i = 0; i < _numberOfMlBlocks; i++)
  {
    double lambSum = 0;
    for (auto itr = _mlBlocks[i].begin(), endItr = _mlBlocks[i].end(); itr != endItr; itr++)
      lambSum += _lambdas[*itr];
    _block_lambSum.push_back(lambSum);
  }
}

void ClusterFinder::initialize_within_distances()
{
  if (!_mlBlocksInitialized)
    initialize_mlBlocks();

  _within_dist.clear();
  _within_dist.reserve(_numberOfMlBlocks);

  int counter = 0;
  for (auto itr1 = _mlBlocks.begin(), endItr1 = _mlBlocks.end(); itr1 != endItr1; itr1++, counter++)
  {
    double within = 0;
    for (auto itr2 = itr1->begin(), endItr2 = itr1->end(); itr2 != endItr2; itr2++)
      for (auto itr3 = itr1->begin(); itr3 != itr2; itr3++)
        within += this->getPointDist(*itr2, *itr3);

    _within_dist.push_back(within);
  }
}

int ClusterFinder::getIndex(int row, int column, int n)
{
  return -((row) * (row + 3) / 2) + row * n + column - 1;
}
