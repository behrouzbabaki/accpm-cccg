#include <iostream>

#include "Oracle.h"
#include "QpGenerator.h"
#include "Parameters.h"
#include "AccpmBlasInterface.h"

#include "instance.hpp"
#include "oracle.hpp"

using std::cout;
using std::endl;
using namespace Accpm;

class WCSSOracleFunction : public OracleFunction
{
  private:
    Instance _instance;
    ClusterFinder _cf;
    bool _first;
    size_t _k;
    size_t _n;

  public:
//    WCSSOracleFunction() : OracleFunction() {}
    WCSSOracleFunction(Instance instance, size_t k) : 
        OracleFunction(), _instance(instance), 
        _cf(_instance.get_ml_pairs(), 
            _instance.get_cl_pairs(), 
            _instance.get_distances())
    {
        _first = true;
        _k = k;
        _n = _instance.get_number_of_instances();
    }

    virtual int eval(const AccpmVector &y, AccpmVector &functionValue,
                     AccpmGenMatrix &subGradients, AccpmGenMatrix *info)
    {

        if (_first)
        {
            functionValue.resize(_k);
            subGradients.resize(_n+1, _k);
            vector< vector< bool > > clusters = _instance.get_initial_clusters();
            vector< double > costs = _instance.get_initial_costs();
            for (size_t j=0; j<_k; j++) {
                functionValue(j) = -costs[j];
                subGradients(0, j) = -1;
                for (size_t i=0; i<_n; i++)
                    subGradients(i+1, j) = double(clusters[j][i]);
            }
            info->resize(_k, 1);
            *info = 0;
            _first = false;
            return 0;
        }

        double *y_arr = y.addr();
        double sigma = y_arr[0];        
        vector< double > lambdas (y_arr+1, y_arr+_n);
        _cf.solve(lambdas, sigma);

        if (!_cf.hasSolution()) /* Add optimality cut, no attractive pattern found for current dual variable y*/
        {
            subGradients = -1;
            subGradients(0, 0) = _k;
            functionValue = AccpmLADotProd(y, subGradients.getColumn(0));
            *info = 1;
        }
        else /* Add feasibility cut, which is the new pattern generated */
        {
            vector< bool > cluster;
            functionValue = _cf.getSolObj(cluster);
            subGradients(0, 0) = -1;
            for (size_t i=0; i<_n; i++)
                subGradients(i+1, 0) = double(cluster[i]);               
            *info = 0;
        }

        return 0;
    }
};

int main(void)
{
    

    Instance instance("data/iris.data",
                      "data/iris.constraints",
                      "data/iris.initial");

    size_t k = 3;

    WCSSOracleFunction f1(instance, k);
    Oracle oracle(&f1);

    char *paramFile = "data/param.txt";
    Accpm::Parameters param(paramFile);
    const char* p = "NumVariables";
    param.setIntParameter(p, instance.get_number_of_instances()+1);

    QpGenerator qpGen;
    qpGen.init(&param, &oracle);

    while (!qpGen.run());

    qpGen.output(cout);
    cout << "The patterns are:" << endl;
    const AccpmGenMatrix *cuts;
    const AccpmVector *x;
    qpGen.getActiveCuts(cuts);
    x = qpGen.getCurrentX();
    cout << *cuts << endl;
    cout << "With weights:" << endl;
    cout << *x << endl;

    qpGen.terminate();

    return 0;
}