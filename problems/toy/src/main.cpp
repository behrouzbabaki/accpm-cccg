
#include <iostream>
#include <vector>

#include "Oracle.h"
#include "Parameters.h"
#include "QpGenerator.h"

using std::cout;
using std::endl;
using std::vector;
using namespace Accpm;

class MyOracleFunction : public OracleFunction {
public:
virtual int eval(const AccpmVector &y,
		 AccpmVector &functionValue,
		 AccpmGenMatrix &subGradients,
		 AccpmGenMatrix *info) {return 0;}
};

int main(void) {
  Parameters p;
  p.setIntParameter("NumVariables", 3);
  vector<double> x;
  for (int i=0; i<3; i++)
    x.push_back(0.12);
  p.setStartingPoint(x);
  
  const AccpmVector *y = p.getStartingPoint();
  cout << y->size() << endl;
  for (int i=0; i<y->size(); i++)
    cout << i << "\t" << y->row(i) << endl;
  cout << "Hello World!" << endl;
  return 0;
}
